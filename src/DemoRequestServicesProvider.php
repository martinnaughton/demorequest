<?php

namespace Galvia\DemoRequest;

use Illuminate\Support\ServiceProvider;

class DemoRequestServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadViewsFrom(__DIR__.'/views', 'demorequest');
        $this->publishes([
            __DIR__.'/views' => resource_path('views/vendor/demorequest'),
        ]);
    }
}
