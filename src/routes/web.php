<?php

use Galvia\DemoRequest\Controllers\DemoRequestController;

Route::resource('/demorequest', DemoRequestController::class);
