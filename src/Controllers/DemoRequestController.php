<?php

namespace Galvia\DemoRequest\Controllers;

use Galvia\DemoRequest\DemoRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DemoRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('demorequest::demo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DemoRequest  $demoRequest
     * @return \Illuminate\Http\Response
     */
    public function show(DemoRequest $demoRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DemoRequest  $demoRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(DemoRequest $demoRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DemoRequest  $demoRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DemoRequest $demoRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DemoRequest  $demoRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(DemoRequest $demoRequest)
    {
        //
    }
}
